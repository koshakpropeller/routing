import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Route, BrowserRouter as Router, Link, useParams} from 'react-router-dom';
let JSON = {
    "Vadim": {
        name: "Вадим",
        description: "Парень с кубиками"
    },
    "Nica": {
        name:"Вероника",
        description:"Обожает пинаколада"
    },
    "Sergey":{
        name:"Сергей",
        description: "Создатель команды"
    },
    "Yuliya": {
        name:"Юля",
        description:"Пчелка, но иногда Рикардо"
    },
    "Misha": {
        name:"Миша",
        description: "Скоро 20(дед)"
    },
    "Nastya": {
        name:"Настя",
        description: "Лисенок"
    },
    "Kristina":{
        name:"Кристина",
        description: "Пьет чай"
    }
}
function Person(props) {
    let {key} = useParams();
    console.log(key);
    if(key in JSON) {
    return (
     <div>
         <p>{JSON[key].name}</p>
         <p>{JSON[key].description}</p>
     </div>
    )
    }
    else {
        return (
            <div>
                Error
            </div>
        )
    }
}
function Home(props) {
    return (
        <div>
            <p>Главная страница Sweet Cookies</p>
            {
                Object.keys(JSON).map((key)=>{
                    return (

                        <Link to={"/" + key} >
                            <div >
                            {JSON[key].name}
                            </div>
                            </Link>
                    )
                })
            }
        </div>
    )
}
ReactDOM.render(
<Router>
<Route exact path="/" component={(props)=><Home/>} />
<Route exact path="/:key" children={<Person/>} />
</Router>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
